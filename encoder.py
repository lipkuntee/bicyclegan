import numpy as np
import torch
import torch.nn as nn
from torch.autograd import Variable


class ResidualBlock(nn.Module):
    def __init__(self, in_channels, out_channels, downsample=False):
        super(ResidualBlock, self).__init__()

        self.main = nn.Sequential(
            nn.Conv2d(in_channels, out_channels, 3, 2 if downsample else 1, 1, bias=False),
            nn.BatchNorm2d(out_channels),
            nn.ReLU(True),
            nn.Conv2d(out_channels, out_channels, 3, padding=1, bias=False),
            nn.BatchNorm2d(out_channels)
        )
        
        self.highway = nn.Identity()
        self.out_activation = nn.ReLU(True)

        if in_channels == out_channels and not downsample:
            return
        
        self.highway = nn.Sequential(
            nn.Conv2d(in_channels, out_channels, 1, 2 if downsample else 1, 0, bias=False),
            nn.BatchNorm2d(out_channels)
        )

    def forward(self, x):
        main = self.main(x)
        skip = self.highway(x)
        return self.out_activation(main + skip)


class ResNet(nn.Sequential):
    def __init__(self, in_channels):
        super(ResNet, self).__init__(
            nn.Conv2d(in_channels, 64, 7, 2, 3, bias=False),
            nn.BatchNorm2d(64),
            nn.ReLU(True),
            nn.MaxPool2d(3, 2, padding=1),
            ResidualBlock(64, 64),
            ResidualBlock(64, 64),
            ResidualBlock(64, 128, True),
            ResidualBlock(128, 128),
            ResidualBlock(128, 256, True),
            ResidualBlock(256, 256),
            ResidualBlock(256, 512, True),
            ResidualBlock(512, 512),
            nn.AdaptiveAvgPool2d((1, 1)),
            nn.Flatten()
        )


class Encoder(nn.Module):
    def __init__(self, in_channels, latent_dim):
        super(Encoder, self).__init__()

        self.feature_extractor = ResNet(in_channels)
        self.fc_mu = nn.Linear(512, latent_dim)
        self.fc_logvar = nn.Linear(512, latent_dim)

    def forward(self, x):
        features = self.feature_extractor(x)
        return self.fc_mu(features), self.fc_logvar(features)


def reparameterization(mu, logvar, latent_dim):
    std = torch.exp(logvar / 2.)
    sampled_z = torch.normal(0., 1., (mu.shape[0], latent_dim)).to(std.get_device())
    z = sampled_z * std + mu
    return z