import torch
import torch.nn as nn


class ChannelAttentionModule(nn.Module):
    def __init__(self, in_channels, reduction=16):
        super(ChannelAttentionModule, self).__init__()
        
        shared_mlp = nn.Sequential(
            nn.Flatten(),
            nn.Linear(in_channels, in_channels // reduction),
            nn.ReLU(True),
            nn.Linear(in_channels // reduction, in_channels),
            nn.Unflatten(1, (in_channels, 1, 1))
        )

        self.max_pool = nn.Sequential(nn.AdaptiveMaxPool2d(1), shared_mlp)
        self.avg_pool = nn.Sequential(nn.AdaptiveAvgPool2d(1), shared_mlp)

    def forward(self, x):
        return torch.sigmoid(self.max_pool(x) + self.avg_pool(x))


class SpatialAttentionModule(nn.Module):
    def __init__(self, in_channels, kernel_size=7):
        super(SpatialAttentionModule, self).__init__()

        self.max_pool = nn.Sequential(
            nn.Unflatten(1, (1, in_channels)),
            nn.MaxPool3d((in_channels, 1, 1)),
            nn.Flatten(1, 2)
        )

        self.avg_pool = nn.Sequential(
            nn.Unflatten(1, (1, in_channels)),
            nn.AvgPool3d((in_channels, 1, 1)),
            nn.Flatten(1, 2)
        )

        self.output_block = nn.Sequential(
            nn.Conv2d(2, 1, kernel_size, padding=kernel_size // 2, padding_mode='reflect'),
            nn.Sigmoid()
        )

    def forward(self, x):
        feature_descriptor = torch.concat((self.max_pool(x), self.avg_pool(x)), dim=1)
        return self.output_block(feature_descriptor)


class BlockAttentionModule(nn.Module):
    def __init__(self, in_channels, reduction=16, kernel_size=7):
        super(BlockAttentionModule, self).__init__()

        self.channel_attention = ChannelAttentionModule(in_channels, reduction)
        self.spatial_attention = SpatialAttentionModule(in_channels, kernel_size)

    def forward(self, x):
        x = x * self.channel_attention(x)
        x = x * self.spatial_attention(x)
        return x


class SeparableConv2d(nn.Sequential):
    def __init__(self, in_channels, out_channels, kernel_size, stride=1, bias=True):
        super(SeparableConv2d, self).__init__(
            nn.Conv2d(in_channels, in_channels, kernel_size, stride, kernel_size//2, bias=bias, groups=in_channels),
            nn.Conv2d(in_channels, out_channels, 1)
        )


class DownsampleBlock(nn.Module):
    def __init__(self, in_channels, out_channels, attention=True):
        super(DownsampleBlock, self).__init__()

        self.attention_block = BlockAttentionModule(in_channels) if attention else None
        self.conv_block = nn.Sequential(
            nn.Conv2d(in_channels, out_channels, 3, padding=1, stride=2, bias=False),
            nn.BatchNorm2d(out_channels, 0.8),
            nn.LeakyReLU(0.2, True)
        )

    def forward(self, x):
        x = self.attention_block(x) if self.attention_block else x
        return self.conv_block(x)


class UpsampleBlock(nn.Module):
    def __init__(self, in_channels, out_channels, dropout=False, attention=True):
        super(UpsampleBlock, self).__init__()

        self.upsample = nn.Upsample(scale_factor=2)
        self.attention_block = BlockAttentionModule(in_channels) if attention else None
        self.conv_block = nn.Sequential(
            *filter(
                None,
                (
                    nn.Conv2d(in_channels, out_channels, 3, padding=1, bias=False),
                    nn.BatchNorm2d(out_channels, 0.8),
                    nn.Dropout2d() if dropout else None,
                    nn.ReLU(True)
                )
            )
        )

    def forward(self, x, skip):
        x = self.upsample(x)
        x = torch.concat((x, skip), dim = 1)
        x = self.attention_block(x) if self.attention_block else x
        return self.conv_block(x)


class Generator(nn.Module):
    def __init__(self, in_channels, out_channels, input_shape, latent_dim=8):
        super(Generator, self).__init__()

        self.noise_block = nn.Sequential(
            nn.Linear(latent_dim, input_shape[0] * input_shape[1]),
            nn.Unflatten(1, (1, *input_shape))
        )

        self.input_block = nn.Sequential(
            nn.Conv2d(in_channels + 1, 64, 3, padding=1),
            nn.LeakyReLU(0.2, True)
        )

        self.output_block = nn.Sequential(
            nn.Conv2d(32, out_channels, 3, padding=1),
        )

        self.encoder = nn.ModuleList([
            DownsampleBlock(64, 128),
            DownsampleBlock(128, 256),
            DownsampleBlock(256, 512),
            DownsampleBlock(512, 512),
            DownsampleBlock(512, 512),
            DownsampleBlock(512, 512),
            DownsampleBlock(512, 512, attention=False)
        ])

        self.decoder = nn.ModuleList([
            UpsampleBlock(1024, 512, dropout=True, attention=False),
            UpsampleBlock(1024, 512, dropout=True, attention=False),
            UpsampleBlock(1024, 512, dropout=True),
            UpsampleBlock(1024, 256),
            UpsampleBlock(512, 128),
            UpsampleBlock(256, 64),
            UpsampleBlock(128, 32),
        ])

    def forward(self, x, noise):
        skips = []

        noise = self.noise_block(noise)
        x = self.input_block(torch.concat((x, noise), dim=1))

        for e in self.encoder:
            skips.append(x)
            x = e(x)

        for d in self.decoder:
            x = d(x, skips.pop())

        return self.output_block(x)