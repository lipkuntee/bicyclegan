import torch
import torch.nn as nn


class MultiDiscriminator(nn.Module):
    def __init__(self, in_channels):
        super(MultiDiscriminator, self).__init__()

        def conv_block(in_channels, out_channels, batchnorm=True):
            return nn.Sequential(
                *filter(None, [
                    nn.Conv2d(in_channels, out_channels, 3, 2, 1, bias=False),
                    nn.BatchNorm2d(out_channels, 0.8) if batchnorm else None,
                    nn.LeakyReLU(0.2, True)
                ])
            )
        
        def generate_discriminator(num_layers):
            blocks = [conv_block(in_channels, 64, False)]
            blocks += [conv_block(min(512, 64 * (2 ** i)), min(512, 128 * (2 ** i))) for i in range(num_layers - 1)]
            return nn.Sequential(
                *blocks, nn.Conv2d(512, 1, 3, padding=1), nn.AdaptiveAvgPool2d((1, 1)), nn.Flatten()
            )

        # C64-C128-C256-C512 (70x70)
        # C64-C128-C256-C512-C512 (140x140)

        self.discriminators = nn.ModuleList([generate_discriminator(num_layers) for num_layers in [4, 5]])

    def forward(self, x):
        return torch.concat([d(x) for d in self.discriminators], dim=1)
