import os
import datetime

import tqdm
import torch
from torch.utils.data import DataLoader
import torchvision.utils as vutils

from config import *
from dataset import CustomDataset
from encoder import Encoder, reparameterization
from generator import Generator
from discriminator import MultiDiscriminator


def weights_init_normal(m):
    classname = m.__class__.__name__

    if classname.find('Conv') != -1:
        torch.nn.init.normal_(m.weight.data, 0.0, 0.02)
    elif classname.find('BatchNorm2d') != -1:
        torch.nn.init.normal_(m.weight.data, 1.0, 0.02)
        torch.nn.init.constant_(m.bias.data, 0.0)


if __name__ == '__main__':
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    torch.backends.cuda.matmul.allow_tf32 = False
    torch.backends.cudnn.benchmark = True
    torch.backends.cudnn.deterministic = False
    torch.backends.cudnn.allow_tf32 = True

    # Models
    encoder = Encoder(IN_CHANNELS, LATENT_DIM).to(device)
    generator = Generator(IN_CHANNELS, OUT_CHANNELS, INPUT_SHAPE, LATENT_DIM).to(device)
    discriminator_vae = MultiDiscriminator(IN_CHANNELS).to(device)
    discriminator_lr = MultiDiscriminator(IN_CHANNELS).to(device)

    # Weight initializations
    generator.apply(weights_init_normal)
    discriminator_vae.apply(weights_init_normal)
    discriminator_lr.apply(weights_init_normal)

    # Optimizers
    encoder_optimizer = torch.optim.Adam(encoder.parameters(), lr=LEARNING_RATE, betas=(BETA_1, BETA_2))
    generator_optimizer = torch.optim.Adam(generator.parameters(), lr=LEARNING_RATE, betas=(BETA_1, BETA_2))
    discriminator_vae_optimizer = torch.optim.Adam(discriminator_vae.parameters(), lr=LEARNING_RATE, betas=(BETA_1, BETA_2))
    discriminator_lr_optimizer = torch.optim.Adam(discriminator_lr.parameters(), lr=LEARNING_RATE, betas=(BETA_1, BETA_2))

    # Scaler
    scaler = torch.cuda.amp.GradScaler()

    # Losses
    l1_loss = torch.nn.L1Loss().to(device)
    mse_loss = torch.nn.MSELoss().to(device)

    # Datasets
    train_dataset = CustomDataset(TRAIN_DATA_PATH, INPUT_SHAPE, HORIZONTAL_FLIP, VERTICAL_FLIP)
    test_dataset = CustomDataset(TEST_DATA_PATH, INPUT_SHAPE)

    # Dataloaders
    train_dataloader = DataLoader(train_dataset, batch_size=BATCH_SIZE, shuffle=True, num_workers=N_WORKERS)
    test_dataloader = DataLoader(test_dataset, batch_size=1, shuffle=True)

    def save_test_samples(filepath):
        real_A, real_B = next(iter(test_dataloader))

        with torch.no_grad():
            noises = torch.normal(0., 1., size=(N_TEST_SAMPLES, LATENT_DIM)).to(device)
            generated = generator(real_A.to(device).repeat((N_TEST_SAMPLES, 1, 1, 1)), noises)
        
        results = vutils.make_grid(torch.concat((real_A, real_B, generated.cpu())))
        results = results * IMAGE_STD + IMAGE_MEAN
        vutils.save_image(results, filepath)

    # Output directory generation
    checkpoint_dir = OUTPUT_DIR + '_' + datetime.datetime.now().strftime('%Y-%m-%d_%H%M%S.%f')
    os.mkdir(checkpoint_dir)
    os.mkdir(checkpoint_dir + '/images')

    # Training loop
    for epoch in range(EPOCH, N_EPOCHS):
        iterations = 0
        samples = 0
        losses = {
            'disc_vae': 0,
            'disc_lr':  0,
            'ge':       0.,
            'pixel':    0.,
            'kl':       0.,
            'latent':   0.,
        }

        for batch in tqdm.tqdm(train_dataloader, desc=f'Epoch {epoch+1:3d}/{N_EPOCHS:3d}'):
            real_A = batch[0].to(device)
            real_B = batch[1].to(device)

            encoder_optimizer.zero_grad(set_to_none=True)
            generator_optimizer.zero_grad(set_to_none=True)

            with torch.cuda.amp.autocast(dtype=torch.float16):
                # cVAE GAN
                mu, logvar = encoder(real_A)
                real_z = reparameterization(mu, logvar, LATENT_DIM)
                fake_B = generator(real_A, real_z)
                fake_scores = discriminator_vae(fake_B)

                pixel_loss = l1_loss(fake_B, real_B)
                kl_loss = 0.5 * torch.sum(torch.exp(logvar) + mu ** 2 - logvar - 1)
                vae_gan_loss = mse_loss(fake_scores, torch.ones_like(fake_scores))

                # cLR GAN
                sampled_z = torch.normal(0., 1., size=(real_A.shape[0], LATENT_DIM)).to(device)
                _fake_B = generator(real_A, sampled_z)
                _fake_scores = discriminator_lr(_fake_B)

                vae_lr_loss = mse_loss(_fake_scores, torch.ones_like(_fake_scores))

                # Encoder loss
                encoder_loss = vae_gan_loss + vae_lr_loss + LAMBDA_PIXEL * pixel_loss + LAMBDA_KL * kl_loss
            
            scaler.scale(encoder_loss).backward(retain_graph=True)
            scaler.step(encoder_optimizer)

            # Generator loss
            with torch.cuda.amp.autocast(dtype=torch.float16):
                _mu, _ = encoder(_fake_B)
                latent_loss = LAMBDA_LATENT * l1_loss(_mu, sampled_z)
            
            scaler.scale(latent_loss).backward()
            scaler.step(generator_optimizer)

            # Discriminator cVAE
            discriminator_vae_optimizer.zero_grad(set_to_none=True)

            with torch.cuda.amp.autocast(dtype=torch.float16):
                real_scores = discriminator_vae(real_B)
                fake_scores = discriminator_vae(fake_B.detach())
                discriminator_vae_loss = mse_loss(real_scores, torch.ones_like(real_scores)) + mse_loss(fake_scores, torch.zeros_like(fake_scores))

            scaler.scale(discriminator_vae_loss).backward()
            scaler.step(discriminator_vae_optimizer)

            # Discriminator cLR
            discriminator_lr_optimizer.zero_grad(set_to_none=True)

            with torch.cuda.amp.autocast(dtype=torch.float16):
                _real_scores = discriminator_lr(real_B)
                _fake_scores = discriminator_lr(_fake_B.detach())
                discriminator_lr_loss = mse_loss(_real_scores, torch.ones_like(_real_scores)) + mse_loss(_fake_scores, torch.zeros_like(_fake_scores))

            scaler.scale(discriminator_lr_loss).backward()
            scaler.step(discriminator_lr_optimizer)

            # Update scaler
            scaler.update()

            # Samples
            iterations += real_A.shape[0]

            if iterations >= SAMPLE_EVERY_ITER:
                generator.eval()
                save_test_samples(f'{checkpoint_dir}/images/epoch-{epoch+1:05d}_iter-{(samples * SAMPLE_EVERY_ITER + iterations):06d}.png')
                generator.train()
                iterations -= SAMPLE_EVERY_ITER
                samples += 1

            # Loss loggings
            losses['disc_vae']  += discriminator_vae_loss.item()
            losses['disc_lr']   += discriminator_lr_loss.item()
            losses['ge']        += encoder_loss.item()
            losses['pixel']     += pixel_loss.item()
            losses['kl']        += kl_loss.item()
            losses['latent']    += latent_loss.item()

        loss_string = 'Losses: cVAE Discriminator = ' + str(losses['disc_vae'])
        loss_string += ' cLR Discriminator = ' + str(losses['disc_lr'])
        loss_string += '\n        Encoder = ' + str(losses['ge'])
        loss_string += ' Pixel = ' + str(losses['pixel'])
        loss_string += ' KL = ' + str(losses['kl'])
        loss_string += ' Latent = ' + str(losses['latent'])

        print(loss_string)

        # Save models
        if (epoch + 1) % SAVE_EVERY_EPOCH == 0:
            torch.save(encoder, f'{checkpoint_dir}/epoch-{epoch+1:05d}_encoder.pth')
            torch.save(generator, f'{checkpoint_dir}/epoch-{epoch+1:05d}_generator.pth')
            torch.save(discriminator_vae, f'{checkpoint_dir}/epoch-{epoch+1:05d}_disc-vae.pth')
            torch.save(discriminator_lr, f'{checkpoint_dir}/epoch-{epoch+1:05d}_disc-lr.pth')

        torch.save(encoder, f'{checkpoint_dir}/last_encoder.pth')
        torch.save(generator, f'{checkpoint_dir}/last_generator.pth')
        torch.save(discriminator_vae, f'{checkpoint_dir}/last_disc-vae.pth')
        torch.save(discriminator_lr, f'{checkpoint_dir}/last_disc-lr.pth')

        with open(f'{checkpoint_dir}/loggings.txt', 'a') as fp:
            fp.write(f'================================== EPOCH {epoch+1:d} ==================================\n')
            fp.write(loss_string + '\n\n')
