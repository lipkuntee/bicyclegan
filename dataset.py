import glob

from PIL import Image
import torch
from torch.utils.data import Dataset
from torchvision import transforms

from config import *


class CustomDataset(Dataset):
    def __init__(self, path, size, flipX=False, flipY=False):
        self.paths = glob.glob(path + '/*.' + FILE_EXTENSION)

        self.to_tensor = transforms.ToTensor()
        transform = [transforms.Resize(size), transforms.Normalize(IMAGE_MEAN, IMAGE_STD)]
        if flipX: transform.append(transforms.RandomHorizontalFlip())
        if flipY: transform.append(transforms.RandomVerticalFlip())
        self.transform = transforms.Compose(transform)

    def __len__(self):
        return len(self.paths)
    
    def __getitem__(self, index):
        image = Image.open(self.paths[index]).convert('RGB')
        w, h = image.size

        if A_TO_B:
            image_A = image.crop((0, 0, w / 2, h))
            image_B = image.crop((w / 2, 0, w, h))
        else:
            image_A = image.crop((w / 2, 0, w, h))
            image_B = image.crop((0, 0, w / 2, h))

        if IN_CHANNELS == 1: image_A = image_A.convert('L')
        if OUT_CHANNELS == 1: image_B = image_B.convert('L')

        image_A = self.to_tensor(image_A)
        image_B = self.to_tensor(image_B)

        stacked = torch.concat((image_A, image_B), dim=0)
        stacked = self.transform(stacked)

        return stacked[:IN_CHANNELS, :, :], stacked[IN_CHANNELS:, :, :]